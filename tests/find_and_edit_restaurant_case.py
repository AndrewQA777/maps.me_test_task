# -*- coding: utf-8 -*-
from appium import webdriver
from selenium.webdriver.support.wait import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time

def test_find_and_edit_restaurant_case(app):
    # TODO Implement Page Object pattern for better maintainability and readability

    # ----- константы, используемые в тесте -----
    # TODO Implement properties
    desired_location = [55.755768, 37.619305, 0]  # желаемое местоположение
    category = "Еда"
    object_name = "Руккола"
    address = "Никольская улица, 8/1 с1"
    latin_name = "Rukkola Latin"
    post_index = "109012"
    from_time = None
    to_time = None
    email = "test@test.com"
    primary_cuisine = "Итальянская кухня"
    secondary_cuisine = "Австрийская кухня"

    # ----- создание драйвера, запуск приложения, передача mock-координат -----
    driver = app.driver
    app.set_location(desired_location)
    # TODO Реализовать проверку текущего местоположения с помощью location_manager

    # ----- Поиск объекта -----
    driver.find_element_by_xpath("//android.widget.ImageView[@resource-id='com.mapswithme.maps.pro:id/search']").click()
    wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
                                                                "//android.widget.TextView[@text='Категории']")).click()
    time.sleep(5)
    wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
                                                 "//android.widget.TextView[@text='" + category + "']")).click()
    results = wait(driver, 60).until(EC.presence_of_all_elements_located((By.XPATH,
                                         "//android.widget.TextView[@resource-id='com.mapswithme.maps.pro:id/title']")))
    assert len(results) > 0
    object_locator = "//android.widget.TextView[@text='" + object_name + "']"
    time.sleep(5)
    wait(driver, 20).until(lambda s: driver.find_element_by_xpath(object_locator)).click()

    # ----- Проверки выбора объекта и перехода к карте -----
    time.sleep(5)
    map = wait(driver, 20).until(EC.visibility_of_element_located((By.XPATH, "//android.widget.RelativeLayout")))
    assert map.is_displayed()
    item_preview = driver.find_element_by_xpath("//android.widget.RelativeLayout[contains(@resource-id,'pp__preview')]")
    assert item_preview.is_displayed()
    # TODO реализовать распознавание изображения стрелки по эталону - не отображается как отдельный элемент
    assert driver.find_element_by_xpath("//android.widget.TextView[contains(@resource-id,'id/tv__title')]").\
                                                                                    get_attribute("text") == object_name
    item_subtitle = driver.find_element_by_xpath("//android.widget.TextView[contains(@resource-id,'id/tv__subtitle')]")
    assert item_subtitle.is_displayed()
    assert "кухня" in item_subtitle.get_attribute("text")
    if address != "" and address is not None:
        address_label = driver.find_element_by_xpath("//android.widget.TextView[contains(@resource-id,'id/tv__address')]")
        assert address_label.get_attribute("text") == address
    my_position_button = driver.find_element_by_xpath("//android.widget.ImageButton[contains(@resource-id,'id/my_position')]")
    assert my_position_button.is_displayed()
    distance_label = driver.find_element_by_xpath("//android.widget.TextView[contains(@resource-id,'id/tv__straight_distance')]")
    assert distance_label.is_displayed()

    # ----- Проверки отображения информации об объекте -----
    driver.find_element_by_xpath("//android.widget.RelativeLayout[contains(@resource-id,'pp__preview')]").click()
    time.sleep(5)
    opening_hours_label = driver.find_element_by_xpath(
                                          "//android.widget.TextView[contains(@resource-id,'id/today_opening_hours')]")
    opening_hours = opening_hours_label.get_attribute("text")
    assert opening_hours != ""  # т.е. в поле действительно что-либо отображается
    phone_number_label = wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
                                              "//android.widget.TextView[contains(@resource-id,'id/tv__place_phone')]"))
    phone_number = phone_number_label.get_attribute("text")
    assert phone_number != ""
    site_label = wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
                                            "//android.widget.TextView[contains(@resource-id,'id/tv__place_website')]"))
    site = site_label.get_attribute("text")
    assert site != ""
    coordinates_label = driver.find_element_by_xpath(
                                             "//android.widget.TextView[contains(@resource-id,'id/tv__place_latlon')]")
    assert "°" not in coordinates_label.get_attribute("text")  # т.е. отображение в десятичном формате
    time.sleep(5)
    coordinates_label.click()
    time.sleep(5)
    assert wait(driver, 20).until(lambda s: "°" in coordinates_label.get_attribute("text")) # т.е. формат с градусами/минутами
    time.sleep(5)
    coordinates_label.click()
    assert wait(driver, 20).until(lambda s: "°" not in coordinates_label.get_attribute("text"))
    coordinates = coordinates_label.get_attribute("text")
    cuisine_label = driver.find_element_by_xpath("//android.widget.TextView[contains(@resource-id,'id/tv__place_latlon')]")
    cuisine = cuisine_label.get_attribute("text")
    assert cuisine != ""

    # ----- Проверки редактирования -----
    time.sleep(10)
    element = wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
        "//android.widget.LinearLayout[contains(@resource-id,'id/ll__place_editor')]"))
    element.click()
    #driver.find_element_by_xpath("//android.widget.TextView[contains(@resource-id,'id/tv__editor')]").click()
    # ---------- Отображение информации об объекте в режиме редактирования -----
    # ef = edit form
    ef_opening_hours_field = wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
                                                "//android.widget.TextView[contains(@resource-id,'id/opening_hours')]"))
    assert ef_opening_hours_field.get_attribute("text") == opening_hours
    ef_phone_number_field = wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
                                              "//android.widget.LinearLayout[@text='Телефон']/android.widget.EditText"))
    # TODO Обсудить с разработчиками - возможно не лучший выбор id т.к. вынуждает привязываться к тексту полей
    assert ef_phone_number_field.get_attribute("text") == phone_number
    ef_site_field = wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
                                              "//android.widget.LinearLayout[@text='Вебсайт']/android.widget.EditText"))
    assert ef_site_field.get_attribute("text") == site
    action = TouchAction(driver)
    action.press(ef_phone_number_field).move_to(ef_opening_hours_field).release().perform()
    ef_email_field = driver.find_element_by_xpath("//android.widget.LinearLayout[@text='Email']/android.widget.EditText")
    ef_cuisine_field = wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
                                                      "//android.widget.TextView[contains(@resource-id,'id/cuisine')]"))
    # ---------- Добавление нового языка и названия на нем, редактирование других полей -----
    driver.find_element_by_xpath("//android.widget.TextView[contains(@resource-id,'id/add_langs')]").click()
    time.sleep(5)
    driver.find_element_by_xpath("//android.widget.TextView[@text='Latin']").click()
    assert wait(driver, 20).until(lambda s: ef_opening_hours_field.is_displayed())  # т.е. отображается окно редактора
    ef_name_field_latin = driver.find_element_by_xpath("//android.widget.LinearLayout[@text='Latin']/android.widget.EditText")
    ef_name_field_latin.send_keys(latin_name)
    post_index_field = driver.find_element_by_xpath("//android.widget.LinearLayout[@text='Почтовый индекс']/android.widget.EditText")
    post_index_field.send_keys(post_index)
    # TODO реализовать изменение часов работы
    ef_email_field.send_keys(email)
    ef_cuisine_field.click()
    additional_cuisine_checkbox_locator = "//android.widget.TextView[@text='" + secondary_cuisine + "']"
    driver.find_element_by_xpath(additional_cuisine_checkbox_locator).click()
    save_button = driver.find_element_by_xpath("//android.widget.ImageView[contains(@resource-id,'id/save')]")
    save_button.click()
    wi_fi_switch = driver.find_element_by_xpath("//android.widget.Switch[contains(@resource-id,'id/sw__wifi')]")
    wi_fi_switch.click()
    save_button = driver.find_element_by_xpath("//android.widget.ImageView[contains(@resource-id,'id/save')]")
    save_button.click()

    # ----- Проверка приглашения к авторизации/регистрации -----
    register_popup = wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
                                                      "//android.widget.Button[contains(@resource-id,'id/register')]"))
    assert register_popup.is_displayed()

    # ----- Проверка отображения измененной информации -----
    map = wait(driver, 20).until(lambda s: driver.find_element_by_xpath("//android.widget.RelativeLayout"))
    map.click()
    wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
                                       "//android.widget.RelativeLayout[contains(@resource-id,'pp__preview')]")).click()
    opening_hours_label = wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
                                          "//android.widget.TextView[contains(@resource-id,'id/today_opening_hours')]"))
    edited_opening_hours = opening_hours_label.get_attribute("text")
    assert (from_time in edited_opening_hours) and (to_time in edited_opening_hours)
    phone_number_label = wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
        "//android.widget.TextView[contains(@resource-id,'id/tv__place_phone')]"))
    phone_number = phone_number_label.get_attribute("text")
    assert phone_number != ""  # не редактировался - просто проверяем, что по-прежнему отображается
    site_label = wait(driver, 20).until(lambda s: driver.find_element_by_xpath(
                                                "//android.widget.TextView[contains(@resource-id,'id/tv__place_website')]"))
    site = site_label.get_attribute("text")
    assert site != "" # не редактировался - просто проверяем, что по-прежнему отображается
    email_label = driver.find_element_by_xpath("//android.widget.TextView[contains(@resource-id,'id/tv__place_email')]")
    cuisine_label = driver.find_element_by_xpath("//android.widget.TextView[contains(@resource-id,'id/tv__place_latlon')]")
    cuisine = cuisine_label.get_attribute("text")
    assert (primary_cuisine in cuisine) and (secondary_cuisine in cuisine)
    wi_fi_status_label = driver.find_element_by_xpath(
                               "//android.widget.TextView[contains(@resource-id,'id/tv__place_wifi') and @text='Есть']")
    assert wi_fi_status_label.is_displayed()



