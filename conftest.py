__author__ = 'Andrew'

from appium import webdriver
from src.fixture.application import Application
import pytest

#fixture = None
#target = None


@pytest.fixture
def app(request):
    desired_caps = {}
    desired_caps['platformName'] = 'Android'
    desired_caps['appiumVersion'] = '1.4.16'
    desired_caps['platformVersion'] = '4.4'
    desired_caps['deviceName'] = 'HIT HT7070MG'
    desired_caps['browserName'] = ''
    desired_caps['app'] = 'C:\\Users\\fcd\\Desktop\\MAPS_ME_Automation\\src\\resources\\application\\android-web-universal-release-6.4.2-160928.apk'
    fixture = Application(desired_caps)

    return fixture
