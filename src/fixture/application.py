from selenium import webdriver

class  Application:
    def __init__(self, desired_caps):
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def set_location(self, desired_location):
        driver = self.driver
        lat = desired_location[0]
        long = desired_location[1]
        alt = desired_location[2]
        driver.set_location(lat, long, alt)
